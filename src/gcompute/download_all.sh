. env.sh

mkdir -p download/output/
gcloud compute scp ${HOST}:~/proj/output/*.* download/output/
mkdir -p download/code/
gcloud compute scp ${HOST}:~/proj/src/r/*.* download/code/
