. env.sh

gcloud compute ssh ${HOST} --command "mkdir -p ~/proj/src/r"

#gcloud compute scp proj/src/r/models_*.Rmd ${HOST}:~/proj/src/r/
#gcloud compute scp proj/src/r/models_*.R ${HOST}:~/proj/src/r/
gcloud compute scp proj/src/r/*.* ${HOST}:~/proj/src/r/
