. env.sh

gcloud compute ssh ${HOST} --command "mkdir -p ~/proj/data/20181205"

# gcloud compute scp proj/data/20181205/Trip_energy_usage_processed_dev.csv ${HOST}:~/proj/data/20181205/
# gcloud compute scp proj/data/20181205/Trip_energy_usage_processed_train.csv ${HOST}:~/proj/data/20181205/
gcloud compute scp proj/data/20181205/Trip_energy_usage.csv ${HOST}:~/proj/data/20181205/
