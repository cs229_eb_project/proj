Neural Network Code Execution:

python run_dev.py  -- Runs with development set from data placed in specified location
python run_test.py -- Runs with teste set from data placed in specified location



Files:

busdata.py						Data reading / loading utilities
nn_utils_trimmed_down.py		NN training, prediction functions
routes.csv						Necessaary file specifying routes to process
run_dev.py						Invokes execution with dev data
run_test.py						Invokes execution with test data
train_and_run.py				Main program, executing with dev data
train_and_run_with_testdata.py	Main program, executing with test data
         
other_utils directory			Files not needed in current incarnation
