import pandas as pd
import io
import gzip

def read_all(data_dir, verbose=False):
	if verbose:
		print('## reading routes, records & normalization data.')
	routes = pd.read_csv('routes.csv'.format(data_dir), index_col=0)
	data_normalize = pd.read_csv('{}/data_normalize.csv'.format(data_dir), index_col=0)
	with gzip.open('{}/data.csv.gz'.format(data_dir)) as fdata:
		data = pd.read_csv(io.TextIOWrapper(fdata, newline=""), index_col=0)
	if verbose:
		print('## > read {0} routes with {1} records'.format(len(routes), data.shape[0]))
	hdrs_x = [i for i in data.columns.values if not i in ['RecordSet','RecordRoute', 'Trip_Energy_Per_Km']]
	hdr_y = 'Trip_Energy_Per_Km'
	if verbose:
		print( 'Dropping records with Trip_Energy_Per_Km > 10.0')
	energyIndex =( data.loc[ :, 'Trip_Energy_Per_Km' ] < 10.0 ).values
	data = data[energyIndex ]
    # Data already filtered for the low Trip_Energy_Kw_Used values, in the incoming dataset
	#if verbose:
	#	print( 'Dropping records with Trip_Energy_Kw_Used   <= 0.05')
	#energyIndex2 =( data.loc[ :, 'Trip_Energy_Kw_Used' ] > 0.05 ).values
	#data = data[energyIndex2 ]
	if verbose: 
		print('## > converting [Month],[Route],[StartHour],[Weekday] to type category')
	data['Month'] = data['Month'].astype('category')
	data['Route'] = data['Route'].astype('category')
	data['StartHour'] = data['StartHour'].apply(lambda x: None if pd.isnull(x) else '{}'.format(int(x))).astype('category')
	# data['Number_of_stops'] = data['Number_of_stops'].astype('int') # pandas does not support NAN with ints
	data['Weekday'] = data['Weekday'].astype('category')
	return (routes['Route'].tolist(), data, data_normalize, hdrs_x, hdr_y)

def get_dataset(busdata, route, keep_route=False, keep_y=False, keep_nan=False, denormalize=False, verbose=False):
	if verbose:
		print('## retrieving records for route {0}'.format(route))
	(routes, data, data_normalize, hdrs_x, hdr_y) = busdata
	if not keep_route or keep_y:
		hdrs_x = list(hdrs_x)
		if not keep_route:
			hdrs_x.remove('Route')
		if keep_y:
			hdrs_x.insert(0, hdr_y)
	route_data = data.loc[(data['RecordRoute']==route)]
	if verbose:
		print('## > found {0} records for route'.format(route_data.shape[0]))
	# if not keep_nan:
	# 	route_data = route_data.dropna(0)
	if denormalize:
		if verbose:
			print('## > ''de''-normaling records')
		dt_mean = data_normalize.loc[(data_normalize['RecordRoute']==route) & (data_normalize['RecordMetric']=='mean')]
		dt_var = data_normalize.loc[(data_normalize['RecordRoute']==route) & (data_normalize['RecordMetric']=='var')]
		for col in hdrs_x:
			if not pd.isnull(dt_var.iloc[0][col]):
				route_data[col] = route_data[col] * dt_var.iloc[0][col]
			if not pd.isnull(dt_mean.iloc[0][col]):
				route_data[col] = route_data[col] + dt_mean.iloc[0][col]
	cnt = route_data.shape[0]
	if not keep_nan:
		route_data = route_data[hdrs_x + [hdr_y,'RecordSet']].dropna(0)
		if verbose:
			print('## > dropping {0} records with NAs'.format(cnt-route_data.shape[0]))
	train = route_data.loc[data['RecordSet']=='Train']
	dev = route_data.loc[data['RecordSet']=='Dev']
	test = route_data.loc[data['RecordSet']=='Test']
	train_x = train[hdrs_x]
	train_y = train[hdr_y]
	dev_x = dev[hdrs_x]
	dev_y = dev[hdr_y]
	test_x = test[hdrs_x]
	test_y = test[hdr_y]
	if verbose:
		print('## > split into train/dev set ({0}/{1})'.format(len(train_y), len(dev_y)))
	return (train_x, train_y, dev_x, dev_y, test_x, test_y)

def get_routes(busdata):
	(routes, data, data_normalize, hdrs_x, hdr_y) = busdata
	return routes

