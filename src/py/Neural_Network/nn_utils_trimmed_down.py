
import numpy as np
import math
import matplotlib
from matplotlib import pyplot as plt
import pandas as ps
import statsmodels.api as sm
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import r2_score
from sklearn.externals import joblib

def predict( nn_model: MLPRegressor, devtest_xdata_array, devtest_ydata_array = None ):
    y_devtest_prediction  = nn_model.predict(devtest_xdata_array)
    nn_dtreport = ''
    if ( devtest_ydata_array is not None ):
        devtest_r_squared = r2_score( devtest_ydata_array, y_devtest_prediction)
        mse_devtest = np.sum(( devtest_ydata_array - y_devtest_prediction ) * \
            ( devtest_ydata_array - y_devtest_prediction )) / devtest_ydata_array.shape[ 0 ]
        nn_dtreport = F'DevTest  y_mean {devtest_ydata_array.mean():{7}.{4}}\t mse {mse_devtest:{9}.{4}}\t'
        nn_dtreport = nn_dtreport + F'Dev/Test Rsq {devtest_r_squared:{9}.{4}}\n'
    return y_devtest_prediction, nn_dtreport

# Future
#def train_model( nn_model: MLPRegressor, 
#              train_xdata_array, 
#              train_ydata_array, 
#              devtest_xdata_array = None, 
#              devtest_ydata_array = None ) :

#    ## Fit the model
#    nn_model.fit( train_xdata_array, train_ydata_array)

#    y_train_prediction, _ = predict( nn_model, train_xdata_array)
#    train_r_squared = r2_score( train_ydata_array, y_train_prediction)
   
#    nnreport = F'NN steps to convergence {len( nn_model.loss_curve_)}\n'
#    mse_train = np.sum(( train_ydata_array - y_train_prediction ) * ( train_ydata_array - y_train_prediction )) / train_ydata_array.shape[ 0 ]
#    nnreport = nnreport + F'Training y_mean {train_ydata_array.mean():{7}.{4}}\t mse {mse_train:{9}.{4}}\t'
#    nnreport = nnreport + F'Training Rsq {train_r_squared:{9}.{4}}\n'
 
#    return nn_model, nnreport, train_r_squared

#def run_trained_model( nn_model: MLPRegressor,
#                       devtest_xdata_array = None, 
#                       devtest_ydata_array = None ) :

#    ## Prediction
#    y_devtest_prediction, nn_dtreport = predict( nn_model, devtest_xdata_array, devtest_ydata_array )

#    return nn_dtreport, y_devtest_prediction 


def train_and_run_model( nn_model: MLPRegressor, 
              train_xdata_array, 
              train_ydata_array, 
              devtest_xdata_array, 
              devtest_ydata_array) :

    ## Fit the model
    nn_model.fit( train_xdata_array, train_ydata_array)


    y_train_prediction, _ = predict( nn_model, train_xdata_array)
    train_r_squared = r2_score( train_ydata_array, y_train_prediction)
   
    nnreport = F'NN steps to convergence {len( nn_model.loss_curve_)}\n'
    mse_train = np.sum(( train_ydata_array - y_train_prediction ) * ( train_ydata_array - y_train_prediction )) / train_ydata_array.shape[ 0 ]
    nnreport = nnreport + F'Training y_mean {train_ydata_array.mean():{7}.{4}}\t mse {mse_train:{9}.{4}}\t'
    nnreport = nnreport + F'Training Rsq {train_r_squared:{9}.{4}}\n'
    
    ## Prediction
    y_devtest_prediction, nn_dtreport = predict( nn_model, devtest_xdata_array, devtest_ydata_array )

    nnreport = nnreport + nn_dtreport
    print( nnreport )

    return nn_model, nnreport, train_r_squared, y_devtest_prediction #, mse_train, mse_devtest, train_r_squared, devtest_r_squared

