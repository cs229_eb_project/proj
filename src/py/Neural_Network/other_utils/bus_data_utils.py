import numpy as np
import matplotlib.pyplot as plt
import pandas as ps
import statsmodels.formula.api as smf
import statsmodels.api as sm
import project_data_utils

# Variables 13 continuous and 5 categorical( 49 categories )
## Obsolete: processed data is slightly different
# "Trip_Avg_Occupancy"              -> col  4 continuous variable
# "Trip_total_number_Of_Boardings"  -> col  2 continuous variable
# "Trip_Departure_Punctuality"      -> col  7 continuous variable
# "Vehicle_Static_Weight"           -> col 10 continuous variable
# "Trip_Km_Travelled_DuringTrip"    -> col 31 continuous variable
# "Syc_Speed_kmh"                   -> col 29 continuous variable
# "Trip_Duration_Hr"                ->        continuous variable
# "Trip_Energy_Per_Km"              -> col 34 continuous variable
# "Hour_Temperature"                -> col 12 continuous variable
# "drivestyle_value"                -> col 14 continuous variable
# "Number_of_stops"                 -> col 15 continuous variable
# "Urban_Avg"                       -> col 16 continuous variable
# "Month"                           -> col 26 categorial variable ( 12 )  mei jul etc
# "Weekday"                         -> col 25 categorial variable (  7 )  ma di wo vr ma za zo do
# "Year"                            ->        categorial variable (  2 )  2017  2018
# "Route"                           -> col 18 categorial variable (  4 )  L401
# "StartHour"                       -> col 22 categorial variable ( 24 )  0-23  '23:55:30'.split(':')[0]


def get_driver_set( driverColumn ):
    """
    Input: Numpy array holding the Hashed_drivers (strings)
    Returns a set of all Hashed_Drivers,
    """
    return set( driverColumn )

def get_driver_map( driverColumn ):
    """
    Creates forward and reverse dictionaries of 
         Hashed_driver strings <--> index (Order found in dataset (zero based))
    
    Input: Numpy ndarray holding the Hashed_drivers (strings )
    Returns driverMap:        index --> Hashed_driver
            driverReverseMap: Hashed_driver Hashed_driver --> index          
    """
    count = 0
    driverReverseMap = { }
    driverMap = { }
    for i in range( len( driverColumn )):
        if (driverColumn[ i ] in driverReverseMap):
            continue
        driverReverseMap[ driverColumn[ i ] ] = count
        driverMap[ count ] =  driverColumn[ i ]
        count = count + 1
    return driverMap, driverReverseMap




def preprocess( dataframe: ps.DataFrame, routeNumberList ):
    """
    Transforms non-numeric fields to single numbers for our bus data
    routeNumberList can be a list or a single route

    """

    # Example of one-hot
    #data_x_train = ps.concat([data_x_train,ps.get_dummies(data_x_train['StartHour'], prefix='StartHour')],axis=1)
    #    and then don't forget to drop the original column
    #data_x_train = data_x_train.drop('StartHour',axis = 1)


    # Ensure we have a list of routes, even if single element passed
    if ( type( routeNumberList ) is not list ): routeNumberList = [ routeNumberList ]

    # Hardwired filter for low energy usage data points
    energies = dataframe.loc[ :, 'Trip_Energy_Kw_Used']
    ev = energies.values
    energyIndexNotTooLow = ( dataframe.loc[ :, 'Trip_Energy_Kw_Used' ] >= 0.05 ).values
    energyIndexNotTooHigh = ( dataframe.loc[ :, 'Trip_Energy_Per_Km' ] <= 5.0 ).values
    okEnergyIndex =  np.logical_and( energyIndexNotTooLow, energyIndexNotTooHigh )
    dataframe = dataframe[ okEnergyIndex]

    # TODO: Way to do this with less copying, etc?
 
    # Accumulate all indexes that correspond to any route in list. Start with zeroth
    # Put 'true' in the list of indexes for every location with a Route of interest
    routesIndex = ( dataframe.loc[ :, 'Route' ] == routeNumberList[ 0 ] ).values
    for i in range( 1,  len( routeNumberList )):
        # Get the array of True, False indications for everything in this Route, accumulate
        indxs = ( dataframe.loc[ :, 'Route' ] == routeNumberList[ i ] ).values 
        print( F'Records before adding {i}th Route: {sum( routesIndex )}')
        # Now update the cumulative routesIndex, flipping False's to True;s for these indxs
        for j in range( len( routesIndex )):
            routesIndex[ j ] = routesIndex[ j ] or indxs[ j ] 
        print( F'Records after adding {i}th Route: {sum( routesIndex )}')

   
    # Now, revise the dataframe to hold only those records with a route of interest
    dataframe = dataframe[ routesIndex ]
    if ( dataframe.shape[ 0 ] == 0 ):
        return dataframe

    routes = dataframe.loc[ :, 'Route'] 
    rvals = routes.values
    rvalsfloat = np.zeros( rvals.shape) - 1.0

    # We're done with the old Route column
    dataframe= dataframe.drop(  'Route', axis = 1  )

    # Questionable to keep route if only one route... also will screw up scaling

    # For non-categorical case:
    # Give numeric value to route entries, 0 thru number of routes - 1
    for i in range( len( routeNumberList )):
        xxx = 0
        for j in range( rvals.shape[ 0 ]):
            if ( rvals[ j ] == routeNumberList[ i ] ):  
                rvalsfloat[ j ] = float( i )

    dataframe = dataframe.assign( Route = rvalsfloat)     

    dvals = dataframe['Date'].values
    yvals = np.zeros_like( dvals ) - 1.0
    mvals = np.zeros_like( dvals ) - 1.0
    for i in range( dvals.shape[ 0 ]):
        yvals[ i ] = float( dvals[ i ][ 0:4 ])
        mvals[ i ] = float(dvals[ i ][ 5:7 ])
    dataframe['Month']= mvals
    dataframe['Year']  = yvals

    dataframe= dataframe.drop(  'Date', axis = 1  )

    return dataframe
    



def load_data( ssvFile, scaleXdata, routeNumberList, nMax : int = 0 ):
    """
    Gets bus data from a file, and if N is not zero, clips the data to N examples
    Returns data_x as DataFrame
            data_y as Series
    """
    # TODO: Just read in all 400,000 records and get rid of the routes not of interest.
    #raw_data = ps.read_csv('../data/processed/Trip_energy_usage_processed_train_Nov10_4_routes_first_500.ssv', sep = ';')
    #raw_data = ps.read_csv(ssvFile, sep = ';')
    raw_data = ps.read_csv(ssvFile)
                          
    # Transform alphabetic fields for use:
    if ( nMax > 0 ):
        raw_data = raw_data.head( nMax + 1 )

    data = preprocess( raw_data, routeNumberList )

    n_route_records_incoming = data.shape[ 0 ]

    ## ENERGY PER KM??
    data_xy = [
        data.loc[ : , 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
        data.loc[ : , 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),        
        data.loc[ : , 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
        ## NO STANDARD DEVIATION data.loc[ : , 'Vehicle_Static_Weight'].replace(',', '.', regex=True).astype(float),
        data.loc[ : , 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
        data.loc[ : , 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),        
        data.loc[ : , 'Trip_Duration_Hr'].replace(',', '.', regex=True).astype(float),
        data.loc[ : , 'Hour_Temperature'].replace(',', '.', regex=True).astype(float),
        data.loc[ : , 'drivestyle_value'].replace(',', '.', regex=True).astype(float),        
        data.loc[ : , 'Number_of_stops'].replace(',', '.', regex=True).astype(float),
        data.loc[ : , 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
        #data.loc[ : , 'Year'],
        data.loc[ : , 'Month'],       
        data.loc[ : , 'Route'],  
        data.loc[ : , 'StartHour'].replace(',', '.', regex=True).astype(float),
        data.loc[:, 'Trip_Energy_Kw_Used'].replace(',', '.', regex=True).astype(float),
        # We put Label column  in here, so when we drop NaNs, X and y rows will stay consistent
        data.loc[:, 'Trip_Energy_Per_Km'].replace(',', '.', regex=True).astype(float)
        ]

    # Make a Dataframe from our data_xy, which is a list of Series objects
    data_xy = ps.concat(data_xy, axis=1)

    print( "Data shape before dropping NaNs ", data_xy.shape )
    # Clean out all NaNs. 
    data_x = data_xy.dropna()
    print( "X Data shape after dropping NaNs ", data_x.shape ) 



    if ( nMax > 0 ):
        data_x = data_x.head( nMax )
    
    # Pull out the y data, and drop it from the x data
    data_y = data_x.loc[ :, "Trip_Energy_Per_Km" ]
    data_x = data_x.drop(  "Trip_Energy_Per_Km", axis = 1  )

    # If this is Training, we need to scale
    xmean = np.zeros( data_x.shape[ 1 ]);
    xsd = np.zeros( data_x.shape[ 1 ]);
    if ( scaleXdata ):
        data_x, xmean, xsd = project_data_utils.scale_dataframe_columns( data_x )
        ####data_x, xmean, xsd = project_data_utils.scale_dataframe_columns( data_x, [
        ####    'Trip_Avg_Occupancy', 
        ####    'Trip_total_number_Of_Boardings',
        ####    'Trip_Departure_Punctuality',
        ####     #'Vehicle_Static_Weight',   # In our 4 routes, this has zero SD for 1st 4 routes
        ####    'Trip_Km_Travelled_DuringTrip',
        ####    'Syc_Speed_kmh',
        ####    'Trip_Duration_Hr',
        ####    'Hour_Temperature',
        ####     #'drivestyle_value',
        ####    'Number_of_stops',
        ####    'Urban_Avg',
        ####    # 'Year',
        ####    'Month',
        ####    'Route',
        ####    'StartHour'
        ####    ])

    data_yscaled, ymean, ysd = project_data_utils.scale_series( data_y )
  
    ysdelta = data_yscaled.values - data_y.values 

    n_route_records_good = data_x.shape[ 0 ]
    return data_x, xmean, xsd, data_y, data_yscaled, n_route_records_incoming, n_route_records_good