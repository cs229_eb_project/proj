
import numpy as np
import math
import matplotlib
from matplotlib import pyplot as plt
import pandas as ps
import statsmodels.api as sm
#from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import r2_score
from sklearn.externals import joblib

def predict( nn_model: MLPRegressor, devtest_xdata_array, devtest_ydata_array = None ):
    y_devtest_prediction  = nn_model.predict(devtest_xdata_array)
    nn_dtreport = ''
    if ( devtest_ydata_array is not None ):
        devtest_r_squared = r2_score( devtest_ydata_array, y_devtest_prediction)
        mse_devtest = np.sum(( devtest_ydata_array - y_devtest_prediction ) * \
            ( devtest_ydata_array - y_devtest_prediction )) / devtest_ydata_array.shape[ 0 ]
        nn_dtreport = F'DevTest  y_mean {devtest_ydata_array.mean():{7}.{4}}\t mse {mse_devtest:{9}.{4}}\t'
        nn_dtreport = nn_dtreport + F'Dev/Test Rsq {devtest_r_squared:{9}.{4}}\n'
    return y_devtest_prediction, nn_dtreport


def run_model( nn_model: MLPRegressor, 
              train_xdata_array, 
              train_ydata_array, 
              devtest_xdata_array = None, 
              devtest_ydata_array = None ) :

    ## Fit the model
    nn_model.fit( train_xdata_array, train_ydata_array)



    y_train_prediction = nn_model.predict(train_xdata_array)
    train_r_squared = r2_score( train_ydata_array, y_train_prediction)


    #y_modelscore_r2= nn_model.score( train_xdata_array, train_ydata_array)
    #print( 'Score --- ', y_modelscore_r2)
    #report = F'Route {routeNumberList} - Training: NN Steps to convergence: { len( nn_model.loss_curve_) }\n\tTraining R^2 {train_r_squared}\t Dev R^2 {dev_r_squared}'
    
    

    nnreport = F'NN steps to convergence {len( nn_model.loss_curve_)}\n'
    mse_train = np.sum(( train_ydata_array - y_train_prediction ) * ( train_ydata_array - y_train_prediction )) / train_ydata_array.shape[ 0 ]
    nnreport = nnreport + F'Training y_mean {train_ydata_array.mean():{7}.{4}}\t mse {mse_train:{9}.{4}}\t'
    nnreport = nnreport + F'Training Rsq {train_r_squared:{9}.{4}}\n'
    

    ## Prediction
    y_devtest_prediction, nn_dtreport = predict( nn_model, devtest_xdata_array, devtest_ydata_array )
    #if ( devtest_xdata_array is not None ):
    #    xxx = 0
    #    y_devtest_prediction  = nn_model.predict(devtest_xdata_array)
    #    np.savetxt( 'ppred.csv', y_devtest_prediction, delimiter=',' )
    #    np.savetxt( 'plabels.csv', devtest_ydata_array, delimiter=',' )
    #    devtest_r_squared = r2_score( devtest_ydata_array, y_devtest_prediction)
    #    mse_devtest = np.sum(( devtest_ydata_array - y_devtest_prediction ) * \
    #        ( devtest_ydata_array - y_devtest_prediction )) / devtest_ydata_array.shape[ 0 ]
    #    nn_dtreport = F'DevTest  y_mean {devtest_ydata_array.mean():{7}.{4}}\t mse {mse_devtest:{9}.{4}}\t'
    #    nn_dtreport = nn_dtreport + F'Dev/Test Rsq {devtest_r_squared:{9}.{4}}\n'

    #    nnreport = nnreport + nn_dtreport
      
    nnreport = nnreport + nn_dtreport
    print( nnreport )

    ## SciKit-Learn NN's keep the history of loss as part of their fit. Convenient
    #print( 'NN Steps  %d  TRAINING ytrainMeanSq %8.3f  mse %8.3f  diff %8.3f ' % 
    #       (len( fittedModel.loss_curve_), ytrainMeanSq, mse_train, mse_train - ytrainMeanSq))
    #print( 'NN Steps  %d  DEV ydevMeanSq %8.3f  mse %8.3f  diff %8.3f ' % 
    #       (len( fittedModel.loss_curve_), ydevMeanSq, mse_dev, mse_dev - ydevMeanSq))
    
    #finalLoss = fittedModel.loss_curve[ -1 ]

    ###### Debug - read and write file
    ###nn_model = None
    ###nn2_model = joblib.load('saved_model.joblib')
    ###y_dev_prediction2  = nn2_model.predict(dev_xdata_array)
    ###y_modelscore_r22= nn2_model.score( train_xdata_array, train_ydata_array)
    ###delta = y_dev_prediction - y_dev_prediction2


    return nn_model, nnreport #, mse_train, mse_devtest, train_r_squared, devtest_r_squared
