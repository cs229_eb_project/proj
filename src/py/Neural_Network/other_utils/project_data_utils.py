import numpy as np
import matplotlib.pyplot as plt
import pandas as ps
import statsmodels.formula.api as smf
import statsmodels.api as sm


def series_and( series1 : ps.Series, series2 : ps.Series ):    
    """
    Compares two StatsModels.Series objects and returns elementwise AND

        Input: Two series with Boolean values
        Returns: Python list with the inputs AND'd
    Ref: StackOverflow 32192163
    [a and b for a, b in zip(x, y)]

    """
    andSeries = [ a and b for a, b in zip( series1, series2 )]
    return andSeries


def series_or( series1 : ps.Series, series2 : ps.Series ):    
    """
    Compares two StatsModels.Series objects and returns elementwise OR

        Input: Two series with Boolean values
        Returns: Python list with the inputs OR'd
    """
    orSeries = [ a and b for a, b in zip( series1, series2 )]
    return orSeries

def scale_dataframe_columns( frame: ps.DataFrame ): #, columnList ):
    #"""
    #Scales a Pandas DataFrame object: centers at zero, scales by std deviation
    #"""
    sdevs = frame.std( axis = 0 )
    # No scaling, if there is only one element
    sdevs[ sdevs == 0.0 ] = 1.0
    means =  frame[: ].mean(axis = 0 )
    scaledframe = frame.copy()
    scaledframe[: ] -= means   # Could be: means[ sdev != 0 ]
    scaledframe[: ] /= sdevs
    return scaledframe, means, sdevs

####def scale_dataframe_columns( frame: ps.DataFrame ) , columnList ):
####    #"""
####    #Scales a Pandas DataFrame object: centers at zero, scales by std deviation
####    #"""
####    sdevs = frame[ columnList ].std()
####    # No scaling, if there is only one element
####    sdevs[ sdevs == 0.0 ] = 1.0
####    means =  frame[columnList ].mean()
####    scaledframe = frame.copy()
####    scaledframe[columnList ] -= means   # Could be: means[ sdev != 0 ]
####    scaledframe[columnList ] /= sdevs
####    return scaledframe, means, sdevs

def scale_series( series: ps.Series ):
    #"""
    #Scales a Pandas Series object: centers at zero, scales by std deviation
    #"""
    sdev = series.std()
    # No scaling, if there is only one element
    if ( sdev == 0.0 ): sdev = 1.0
    mean =  series.mean()
    scaledSeries = series.copy()
    scaledSeries -= mean
    scaledSeries /= sdev
    return scaledSeries, mean, sdev


def one_hot( data, N ):
    """
    N:    Number of categories
    data: Scalar or 1D array of ints between 1 and N inclusive
          ( inputs are changed to ints & clamped  )
    return: 2D 1-hot array with one row for each input
            ( category 1 is index 0, category N is index N-1 )
    """
    data = data.squeeze().astype( int )
    data = np.atleast_1d( data )
    np.clip( data, 1, N -1 )
    oneHot = np.zeros((data.shape[0], N), dtype = int)
    # TODO: Vectorize?
    for i in range( data.shape[ 0 ]) :
        oneHot[ i, data[ i ] - 1 ] = 1 
    return oneHot

