
import json
import time
import string
import os

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn import datasets, linear_model
from sklearn.neural_network import MLPRegressor
from sklearn import metrics
from sklearn.metrics import r2_score
from sklearn.externals import joblib

import busdata
import nn_utils_trimmed_down
import random

# Program executes Neural networks (hardwired in code ) on test data
# (location hardwired in code) and writes results, and trained models,
# to ./output/ directory with unique run IDs to correlate the different
# outputs across all runs


def main():

    # Setup: All our files will be put in ./output
    if not os.path.exists('output'):
        os.mkdir('output')

    # For engineering purposes, running log of all runs, with minimal info
    if not os.path.exists('nn_cum_dev_runlog.csv'):
        #reportFile = 'output/' + uniqueRouteID + '_' + 'report.txt'
        with open('nn_cum_dev_runlog.csv', 'w' ) as logfile:
            print('run_id,route,train_rsq,dev_rsq,nn_shape,alpha_reg,d2,d3,d4,iterations,sec ', file = logfile)

    ####################### READ DATA #########################

    data = busdata.read_all( "../../_Proprietary_Dec06_Folder", verbose=True)  
    #################### PROCESS ROUTES ########################

    


    ########################## MODEL ###########################

    # Note: It seems all hidden layers have to have the same activation (???)
    # Here we define a Scikit-Learn neural network for regression
    networkShape  = [   (15, 9, 9,7, 5 ),  (11, 7, 7,5, 5 ),     (11, 7, 5, 3 )   ]
    activ         = [      'relu',            'relu',            'relu'           ]
    solvr         = [      'adam',             'adam',             'adam'            ]
    lrn_rate      = [      'adaptive',        'adaptive',        'adaptive'       ]
    maxIter       = [       3000,              3000,             3000             ]
    alph          = [      .1,               .20,               .30              ]
    lr_init       = [      .001,              .001,              .001             ]


    #networkShape  = [ (15, 9, 9,7, 5 ) ,  (15, 9, 9,7, 5 ) , (15, 9, 9,7, 5 ) ,  (15, 9, 9,7, 5 ) ,  (15, 9, 9,7, 5 )  ]
    #activ         = [    'relu'        ,     'relu'        ,    'relu'        ,     'relu'        ,     'relu'         ]
    #solvr         = [    'adam'        ,     'adam'        ,    'adam'        ,     'adam'        ,     'adam'         ]
    #lrn_rate      = [    'adaptive'    ,     'adaptive'    ,    'adaptive'    ,     'adaptive'    ,     'adaptive'     ]
    #maxIter       = [     1000         ,      1000         ,     1000         ,      1000         ,      1000          ]
    #alph          = [    .0005         ,     .001          ,    .005          ,     .01           ,     .05           ]
    #lr_init       = [    .001          ,     .001          ,    .001          ,     .001          ,     .001           ]
    #choice = 0

    #networkShape  = [ (15, 9, 9,7, 5 ) ,  (15, 9, 9,7, 5 ) , (15, 9, 9,7, 5 ) ,  (15, 9, 9,7, 5 ) ,  (15, 9, 9,7, 5 )  ]
    #activ         = [    'relu'        ,     'relu'        ,    'relu'        ,     'relu'        ,     'relu'         ]
    #solvr         = [    'adam'        ,     'adam'        ,    'adam'        ,     'adam'        ,     'adam'         ]
    #lrn_rate      = [    'adaptive'    ,     'adaptive'    ,    'adaptive'    ,     'adaptive'    ,     'adaptive'     ]
    #maxIter       = [     1000         ,      1000         ,     1000         ,      1000         ,      1000          ]
    #alph          = [    .1            ,     .2            ,    .3            ,     .4           ,     .5           ]
    #lr_init       = [    .001          ,     .001          ,    .001          ,     .001          ,     .001           ]
    #choice = 0

    ########################## /MODEL ###########################


    # Create a text string representing each NN topology for simple reporting
    # networkLayers will have the layer sizes separated by a ~
    networkLayers = []
    for i in range(len( networkShape )):
        networkLayers.append( '')
        ni = networkShape[i]
        for j in range( len( ni ) ):
            if ( j == 0 ):
                networkLayers[ i ] = networkLayers[ i ] + str( networkShape[ i ][j])
            else:
                networkLayers[ i ] = networkLayers[ i ] + '~' + str( networkShape[ i ][j])
    
    run_count = 0 
    run_list_count = len( networkShape ) * len( busdata.get_routes(data))
    for imod in range( len( networkShape )):

        nn_model = MLPRegressor( verbose = True,
                                    random_state       = 82855, 
                                    #shuffle            = False,
                                    hidden_layer_sizes = networkShape[ imod ],
                                    #activation         = activ      [ imod ],
                                    activation         = activ       [ imod ],
                                    solver             = solvr       [ imod ],
                                    learning_rate      = lrn_rate    [ imod ],
                                    max_iter           = maxIter     [ imod ], 
                                    learning_rate_init = lr_init     [ imod ],
                                    alpha              = alph        [ imod ], 
                                    tol                = 5e-7)


        # Create unique name for output files
        timeid = time.strftime( '%Y%m%d_%H%M%S_')
        rndltrs = ''.join( random.choices(string.ascii_lowercase, k=3))
        uniqueID = timeid + rndltrs + '_m' + str( imod )

        # Set up output file incremental recording performance of all routes with this model, and 
        # accumulate text and JSON for later writeout of the JSON
        textPerformanceFile = 'output/' + uniqueID + '_' + 'routes_performance.txt'
        performanceString = 'route,train_rsq,dev_rsq,nn_shape,iterations, sec'
        with open( textPerformanceFile, 'w+' ) as text_perf_file:
            print( F'{performanceString}', file = text_perf_file )
        
        jsonPerfList = []
        for route in (busdata.get_routes(data)):
            run_count += 1
            print( F'*** Starting run {run_count} of {run_list_count}')

            uniqueRouteID = uniqueID + '_' + str( route )
        
            (train_x, train_y, dev_x, dev_y, test_x, test_y) = busdata.get_dataset(data, route, verbose=True)
    
            train_x = pd.get_dummies( train_x )
            dev_x = pd.get_dummies(dev_x )

            nn_parameters = nn_model.get_params();

            print( F'NN Layers {nn_parameters[ "hidden_layer_sizes"]}')
            report = F'Route(s) {str( route )} \nRun ID [{uniqueID}]\n' 
    
            # hdrs report = report + F'Columns {strColumns}\n'
            #report = report + F'Training Incoming records {trnrec_in}\t Good records {trnrec_good}\n'
            #report = report + F'Dev/Test Incoming records {dtnrec_in}\t Good records {dtnrec_good}\n'
            report = report + F'Neural Network Topology {nn_parameters[ "hidden_layer_sizes"]}\n'
            report = report + F'Model Settings {nn_parameters}\n'

            report = report + F'Shapes: train_X {train_x.shape}, train_y {train_y.shape}, dev_X {dev_x.shape}, dev_y {dev_y.shape}\n'

            # Run the model: the first set of parameters in the array
            start_time = time.time()
            nn_model, data_section, train_r2_score, yprediction = \
                nn_utils_trimmed_down.train_and_run_model( nn_model, 
                                    train_x, 
                                    train_y,
                                    dev_x, 
                                    dev_y )
            elapsed = int( time.time() - start_time )

            report = report + data_section
            dev_r2_score = r2_score( dev_y, yprediction)
            iterations = len(nn_model.loss_curve_)

            # Output Text / JSON files 

            # Create structure for JSON report
            routeScores = {}
            routeScores[ str( route )] = { 'Train_Rsq'     : train_r2_score,
                                           'Dev_Rsq'       : dev_r2_score,
                                           'Train_records' : train_y.size,
                                           'Dev_records'   : dev_y.size,
                                           'NN_Iterations' : iterations
                                         }

            # Update the ongoing performance string
            jsonPerfList.append( routeScores )

            # Formatting: {x:{width}.{decimals}}
            parms = nn_model.get_params()
            alpha = parms[ 'alpha']

            performanceString = str( route ) + ',' + F'{train_r2_score:{7}.{5}}' + ',' + \
                F'{dev_r2_score:{7}.{5}}'  + ',' + networkLayers[ imod ] + ',' + \
                str( alpha ) + ',' + str( iterations ) + ',' + str( elapsed )
            with open( textPerformanceFile, 'a' ) as text_perf_file:
                print( F'{performanceString}', file = text_perf_file )

            # Save y dev values and Plot prediction vs. actual
            filename = 'output/' + uniqueRouteID + '_Prediction.txt'
            hdr = str( route ) + ' label, prediction' 
            np.savetxt( filename,np.column_stack((dev_y, yprediction)), header = hdr )

            # Update the cumulative log file 
            cumLogEntry = uniqueRouteID + ',' + str( route ) + ',' +   F'{train_r2_score:{7}.{5}}' + ',' + \
                F'{dev_r2_score:{7}.{5}}' + ',' +\
                str( networkLayers[ imod ]) + ',' + str( alpha ) + ',' + ',' + ',' + ',' + str( iterations ) + ',' + str( elapsed )
            #nn_logfile.write( logEntry )
            with open('nn_cum_dev_runlog.csv', "a") as text_file: print( cumLogEntry, file= text_file)

            # Write out the report for this run
            reportFile = 'output/' + uniqueRouteID + '_' + 'report.txt'
            with open(reportFile, "w") as text_file: print(report, file=text_file)
   
            # Figures

            #ypr_df = pd.DataFrame( yprdict  )
            plt.figure()
            plt.scatter( dev_y,yprediction , s = 10 )
            plt.title( str( route ) + ' (Run ID ' + rndltrs + ')  Kwh/Km Predictions')
            plt.ylabel( 'ypred')
            plt.xlabel( 'label')
            #ypr_df.plot( 'y_pred', 'y_label', s = 10, title = ('Route(s) ' + str( route )), kind = 'scatter')
            filename = 'output/' + uniqueRouteID + '_Prediction_' + str( plt.gcf().number ) + '.jpg'
            plt.savefig( filename )
            #plt.show( block = True)
            plt.close()

            plt.figure()
            plt.plot( np.arange( len(nn_model.loss_curve_)-1), nn_model.loss_curve_[1:])
            plt.title( str( route ) + ' (Run ID ' + rndltrs+ ')  Loss vs Iteration' )
            plt.xlabel( 'Neural Net Iteration')
            plt.ylabel( 'Loss')
            plt.yscale( 'log')


            filename = 'output/' + uniqueRouteID + '_Training_' + str( plt.gcf().number ) + '.jpg'
            plt.savefig( filename )
            #plt.show( block = True)
            plt.close()

            # Save this tuned model for later reference
            #    Here is how we later read: clf = joblib.load('filename.joblib') 
            modelfile = 'output/' + uniqueRouteID + '_model.joblib'
            joblib.dump(nn_model,modelfile) 


        jsonPerformanceFile = 'output/' + uniqueID + '_' + 'routes_performance.json'

        #with open( textPerformanceFile, 'w' ) as text_perf_file:
        #    print( F'{performanceString}', file = text_perf_file )

        with open( jsonPerformanceFile, 'w+') as jfile:
            json.dump( jsonPerfList, jfile )


        xxx = 0
##########################################################