import numpy as np
import pandas as ps
import statsmodels.api as sm
import os

def regressie(data_x,data_y,test_x,test_y,save_results=True):
    # Modelleren regressie
    # df_x = ps.DataFrame(data_x, columns=list(data_x.columns))
    # df_x = sm.add_constant(df_x)
    # df_y = ps.DataFrame(data_y, columns=list(data_y.columns))
    model = sm.OLS(data_y, data_x).fit()
    predictions = model.predict(test_x)
    errors_test = predictions - test_y
    # mse = sum(errors_test^2)
    # Print out the statistics
    print(model.summary())

    pvals = model.pvalues
    coeff = model.params
    conf_lower = model.conf_int()[0]
    conf_higher = model.conf_int()[1]
    sderrors = model.bse
    varnames = model.model.exog_names

    results_df = ps.DataFrame({"pvals":pvals,
                               "coeff":coeff,
                               "conf_lower":conf_lower,
                               "conf_higher":conf_higher,
                               "sderrors": sderrors
                                })

    #Reordering...
    results_df = results_df[["coeff","pvals","conf_lower","conf_higher","sderrors"]]
    results_df['varnames'] = list(data_x.columns)
    results_df.columns = ["coeff","pvals","conf_lower","conf_higher","sderrors","varnames"]
    if save_results:
        results_df.to_csv(os.path.join('C:/Users/matth/PycharmProjects/ZE/data/2 Output', 'regressie_output.csv'))
    return results_df, model, errors_test


def regressie_sklearn(data_x,data_y,save_results=True):
    # Modelleren regressie
    df_x = ps.DataFrame(data_x)
    df_y = ps.DataFrame(data_y)
    model = sm.OLS(df_y, df_x).fit()
    # predictions = model.predict(data_x_dev)