from src.py.Random_forest.MB import parse_data
from src.py.Random_forest.MB_OLS import regressie
import matplotlib.pyplot as plt
import pandas as ps
import os
import seaborn as sns
import statsmodels.api as sm
from sklearn import linear_model
sns.set(style="ticks", color_codes=True)

#Set route of interest
route = 'M194'
route_list = ['M180', 'M181', 'L401', 'L402', 'L403', 'L404', 'L405', 'L406', 'L407', 'M090', 'M185', 'M186',
                       'M187', 'M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']
filter_route = True
plt.style.use('fivethirtyeight')

# data_x_train, data_y_train, all_data_train, feature_list = parse_data("C:/Users/matth/PycharmProjects/ZE/data/0 Raw/Trip_energy_usage_processed_train.csv", route_list,
#                                                       filter_route=False,fair_variables=False)
# data_x_dev, data_y_dev, all_data_test, feature_list_test = parse_data("C:/Users/matth/PycharmProjects/ZE/data/0 Raw/Trip_energy_usage_processed_dev.csv", route_list,
#                                                        filter_route=False, fair_variables=False)

data_x_train, data_y_train, all_data_train, feature_list = parse_data("C:/Users/matth/PycharmProjects/ZE/data/0 Raw/Trip_energy_usage_processed_train.csv", route,
                                                     filter_route=True,fair_variables=False)
data_x_dev, data_y_dev, all_data_test, feature_list_test = parse_data("C:/Users/matth/PycharmProjects/ZE/data/0 Raw/Trip_energy_usage_processed_dev.csv", route,
                                                      filter_route=True, fair_variables=False)

all_data_train.to_csv(os.path.join("C:/Users/matth/PycharmProjects/ZE/data/2 Output", 'all_data_train.csv'), index=False)
print(data_x_train.shape)
print(data_y_train.shape)
print(all_data_train.shape)

# corr_plots = sns.pairplot(all_data_train, x_vars=["verbruik","Nr of Boardings","Avg Occupancy","Distance Trip (KM)","Avg Speed",
#                                      "Avg Urbanisation","Punctuality (depart)","Trip duration","Hour_Temperature",
#                                      "drivestyle_value","Number of stops"], y_vars=["verbruik"])
#
# #corr_plots.savefig(os.path.join('../data/2 Output', 'all_data_train_corr_plots' + route + '.png'))
# corr_plots.savefig(os.path.join('../data/2 Output', 'all_data_train_corr_plots.png'))

# plt.show()


#Train eerste regressie met alle dummies
data_x_train = ps.DataFrame(data_x_train, columns=list(feature_list))
data_x_train = sm.add_constant(data_x_train)
data_y_train = ps.DataFrame(data_y_train, columns=['verbruik'])

data_x_test = ps.DataFrame(data_x_dev, columns=list(feature_list_test))
data_x_test = sm.add_constant(data_x_test)
data_y_test = ps.DataFrame(data_y_dev, columns=['verbruik'])

results_df, model, errors_test = regressie(data_x_train,data_y_train, data_x_test, data_y_test, save_results= True)

#En zonder dummies
# data_x_train = ps.DataFrame(data_x_train, columns=list(feature_list))
# data_x_train = sm.add_constant(data_x_train)
# data_x_train = data_x_train.iloc[:,1:11]
# data_y_train = ps.DataFrame(data_y_train, columns=['verbruik'])
#
# data_x_test = ps.DataFrame(data_x_dev, columns=list(feature_list_test))
# data_x_test = sm.add_constant(data_x_test)
# data_x_test = data_x_test.iloc[:,1:11]
# data_y_test = ps.DataFrame(data_y_dev, columns=['verbruik'])
#
# results_df, model, errors_test = regressie(data_x_train,data_y_train, data_x_test, data_y_test, save_results= True)
############

#Plotten van coefficientenplot
results_df = results_df.iloc[2:]
fig, ax = plt.subplots(figsize=(8, 5))
results_df.plot(x='varnames', y='coeff', kind='bar',
             ax=ax, color='none',
             yerr='sderrors', legend=False)
ax.set_ylabel('')
ax.set_xlabel('')
ax.scatter(x=ps.np.arange(results_df.shape[0]),
           marker='s', s=20,
           y=results_df['coeff'], color='black')
ax.axhline(y=0, linestyle='--', color='black', linewidth=2)
ax.xaxis.set_ticks_position('none')
plt.show()
fig.savefig(os.path.join('C:/Users/matth/PycharmProjects/ZE/data/2 Output', 'coefficienten_met_error_met_dummies.png'))

#_ = ax.set_xticklabels(['Agriculture', 'Exam', 'Edu.', 'Catholic', 'Infant Mort.'],
#                       rotation=0, fontsize=16)