import numpy as np
import pandas as ps
import statsmodels.formula.api as smf
import statsmodels.api as sm
import sklearn as sk
from sklearn.impute import SimpleImputer
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from datetime import datetime as dt
import matplotlib.pyplot as plt
import os
import seaborn as sns
sns.set(style="ticks", color_codes=True)

import csv

def parse_data(filename, route, filter_route=True):
    #Takes: datafile
    #Returns: numpy array of datafile, formatted / parsed

    route_base_list = ['M180', 'M181', 'L401', 'L402', 'L403', 'L404', 'L405', 'L406', 'L407', 'M090', 'M185', 'M186',
                       'M187', 'M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']

    data = ps.read_csv(filename, parse_dates=['Date'])
#    ps.set_option('display.max_columns', 500)

    data['Date'] = ps.to_datetime(data['Date'], format='%Y-%m-%d', errors='coerce')

    data['Month'] = ps.DatetimeIndex(data['Date']).month
    data['Weekday'] = ps.DatetimeIndex(data['Date']).weekday

    # Filter regels waarbij energy per km is te laag
    data = data.loc[data['Trip_Energy_Kw_Used'] > 0.05]
    data = data.loc[data['Trip_Energy_Per_Km'] < 10]

    # Let op, for M (Amsterdam) zijn er in Augustus maar 10 regels -> eruit halen
    if route[0] == 'M':
        data = data.loc[data['Month'] != 8]
    else:
        pass

    print("Aantal regels voor NaN drop: %s,%s" % (data.shape))
    data = data.dropna()
    print("Aantal regels na NaN drop: %s,%s" % (data.shape))

    if filter_route:
        data = data.loc[data['Route'] == route]
        data = [
            data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
            data.loc[:, "Trip_Energy_Per_Km"].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'StartHour'],
            data.loc[:, 'Trip_Duration_Hr'],
            data.loc[:, 'Hour_Temperature'],
            data.loc[:, 'drivestyle_value'],
            data.loc[:, 'Number_of_stops'],
            data.loc[:, 'Month'],
            data.loc[:, 'Weekday']]
    else:
        data = data.loc[data['Route'].isin(route_base_list)]
        data = [
            data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
            data.loc[:, "Trip_Energy_Per_Km"].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'StartHour'],
            data.loc[:, 'Trip_Duration_Hr'],
            data.loc[:, 'Hour_Temperature'],
            data.loc[:, 'drivestyle_value'],
            data.loc[:, 'Number_of_stops'],
            data.loc[:, 'Month'],
            data.loc[:, 'Weekday'],
            data.loc[:, 'Route']
        ]


    #        data.loc[:, 'Trip_Departure_OnTime'],
    #        data.loc[:, 'Vehicle_Static_Weight'],
    #    data.loc[:, 'Route'],
    #    data.loc[:, 'Direction'],
    #        data.loc[:, 'Urban_Avg'],

    data = ps.concat(data, axis=1)
    data = data.rename(index=str, columns={"Trip_total_number_Of_Boardings": "Nr of Boardings", "Trip_Avg_Occupancy": "Avg Occupancy", "Syc_Speed_kmh": "Avg Speed   "
        , "Urban_Avg": "Avg Urbanisation", "Trip_Departure_Punctuality": "Punctuality (depart)", "Trip_Duration_Hr": "Trip duration"
        , "Number_of_stops": "Number of stops", "Trip_Km_Travelled_DuringTrip": "Distance Trip (KM)"})
    data_x = data.loc[:, data.columns != "Trip_Energy_Per_Km"]
#    data_x = ps.concat(data_x, axis=1)
    # data_x_train = sm.add_constant(data_x_train, has_constant="skip")
    data_y = data.loc[:, "Trip_Energy_Per_Km"]

    data_x = ps.concat([data_x, ps.get_dummies(data_x['StartHour'], prefix='StartHour')], axis=1)
    data_x = ps.concat([data_x, ps.get_dummies(data_x['Month'], prefix='Month')], axis=1)
    data_x = ps.concat([data_x, ps.get_dummies(data_x['Weekday'], prefix='Weekday')], axis=1)
    # data_x = ps.concat(
    #     [data_x, ps.get_dummies(data_x['Trip_Departure_OnTime'], prefix='Trip_Departure_OnTime')], axis=1)
    data_x = data_x.drop('StartHour', axis=1)
    data_x = data_x.drop('Month', axis=1)
    data_x = data_x.drop('Weekday', axis=1)

    if filter_route:
        pass
    else:
        data_x = ps.concat([data_x, ps.get_dummies(data_x['Route'], prefix='Route')], axis=1)
        data_x = data_x.drop('Route', axis=1)
    # data_x = data_x.drop('Trip_Departure_OnTime', axis=1)
#    print(data_x.head(5))

    # Get the headers as a feature list
    feature_list = list(data_x.columns)


    data_x = data_x.values
    data_y = data_y.values

    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    data_x = imp.fit_transform(data_x)

    return data_x, data_y, feature_list

def train_rf(data_x_train,data_y_train, param_set):


    rf = RandomForestRegressor(n_estimators=param_set['n_estimators'], random_state=42,
                                min_samples_split=param_set['min_samples_split'], min_samples_leaf=param_set['min_samples_leaf'],
                                max_features=param_set['max_features'], max_depth=param_set['max_depth'], bootstrap=param_set['bootstrap'], n_jobs=1)
    # rf_random.best_params_
    rf.fit(data_x_train, data_y_train)

    return rf

def test_and_metrics_rf(rf, data_x_train, data_y_train, data_x_dev, data_y_dev):

    predictions_train = rf.predict(data_x_train)
    predictions_dev = rf.predict(data_x_dev)
    errors_train_r2 = metrics.r2_score(data_y_train, predictions_train)  # abs(predictions - data_y_dev)
    errors_dev_r2 = metrics.r2_score(data_y_dev, predictions_dev)  # abs(predictions - data_y_dev)

    return errors_train_r2, errors_dev_r2


def run_rf(route, filter_route=True):
    data_x_train, data_y_train, feature_list = parse_data("../data/0 Raw/Trip_energy_usage_processed_train.csv", route,
                                                          filter_route)
    data_x_dev, data_y_dev, feature_list_test = parse_data("../data/0 Raw/Trip_energy_usage_processed_dev.csv", route,
                                                           filter_route)

    print("data loaded, starting to train")

    n_estimators = [[int(x) for x in np.linspace(start=2, stop=48, num=12)]]
    n_estimators.append([int(x) for x in np.linspace(start=50, stop=100, num=3)])
    n_estimators.append([250,500,1000])
    # n_estimators.append([250,500,1000,2000,3000,5000,10000])
    n_estimators = [item for sublist in n_estimators for item in sublist]
    # Number of features to consider at every split
    # max_features = ['auto',8,10,12,13,14,15,16,18,20,22,24]
    max_features = ['auto',8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,28,30]
    # Maximum number of levels in tree
    max_depth = [[int(x) for x in np.linspace(2, 10, num=5)]]
    max_depth.append([12, 15, 20, 40, 80])
    max_depth.append([None])
    max_depth = [item for sublist in max_depth for item in sublist]
    # Minimum number of samples required to split a node
    min_samples_split = [2, 3, 4, 5, 6, 8, 10, 15, 20]
    # Minimum number of samples required at each leaf node
    min_samples_leaf = [1, 2, 3, 4, 5, 6, 8, 10, 20, 40, 100]
    # Method of selecting samples for training each tree
    bootstrap = [True, False]

    # Create the random grid
    param_grid = {'n_estimators': n_estimators,
                  'max_features': max_features,
                  'max_depth': max_depth,
                  'min_samples_split': min_samples_split,
                  'min_samples_leaf': min_samples_leaf,
                  'bootstrap': bootstrap}

    param_set = {'n_estimators': 250,
                  'max_features': 16,
                  'max_depth': 40,
                  'min_samples_split': 5,
                  'min_samples_leaf': 2,
                  'bootstrap': True}
    nr_estimator_list = []
    max_features_list = []
    max_depth_list = []
    min_samples_split_list =[]
    min_samples_leaf_list = []

    for item in param_grid['n_estimators']:
        param_set['n_estimators'] = item
        rf = train_rf(data_x_train, data_y_train, param_set)
        errors_train_r2, errors_dev_r2 = test_and_metrics_rf(rf, data_x_train, data_y_train,
                                                             data_x_dev, data_y_dev)
        nr_estimator_list.append([item, errors_dev_r2])

    estimator_out = ps.DataFrame.from_records(nr_estimator_list, columns=['Parameter', 'R2'])
    estimator_out.to_csv(os.path.join('../data/2 Output/grid_search', 'Nr_Estimator_r2_' + route + '.csv'), index=False)
    # estimator_out.to_csv(os.path.join('../data/2 Output/grid_search', 'Nr_Estimator_r2_' + route + '.csv'), index=False)

    plot = sns.jointplot(x="Parameter", y="R2", data=estimator_out)
    plot.fig.suptitle("Nr_Estimator_r2_")
    plot.savefig(os.path.join('../data/2 Output/grid_search', 'Nr_Estimator_r2_' + route + '.png'))
    # plot.savefig(os.path.join('../data/2 Output/grid_search', 'Nr_Estimator_r2_' + route + '.png'))


    param_set['n_estimators'] = 250

    for item2 in param_grid['max_features']:
        param_set['max_features'] = item2
        if param_set['max_features'] == 'auto':
            rf = train_rf(data_x_train, data_y_train, param_set)
        else:
            if param_set['max_features'] > 16:
                try:
                    rf = train_rf(data_x_train, data_y_train, param_set)
                except:
                    param_set['max_features'] = 16
                    rf = train_rf(data_x_train, data_y_train, param_set)

        errors_train_r2, errors_dev_r2 = test_and_metrics_rf(rf, data_x_train, data_y_train,
                                                             data_x_dev, data_y_dev)
        max_features_list.append([item2, errors_dev_r2])

    estimator_out = ps.DataFrame.from_records(max_features_list, columns=['Parameter', 'R2'])
    estimator_out.to_csv(os.path.join('../data/2 Output/grid_search', 'Max_features_r2_' + route + '.csv'), index=False)
    plot = sns.catplot(x="Parameter", y="R2", data=estimator_out)
    plot.fig.suptitle("Max_features_r2_")
    plot.savefig(os.path.join('../data/2 Output/grid_search', 'Max_features_r2_' + route + '.png'))

    param_set['max_features'] = 16


    for item3 in param_grid['max_depth']:
        param_set['max_depth'] = item3
        rf = train_rf(data_x_train, data_y_train, param_set)
        errors_train_r2, errors_dev_r2 = test_and_metrics_rf(rf, data_x_train, data_y_train,
                                                             data_x_dev, data_y_dev)
        max_depth_list.append([item3, errors_dev_r2])

    estimator_out = ps.DataFrame.from_records(max_depth_list, columns=['Parameter', 'R2'])
    estimator_out.to_csv(os.path.join('../data/2 Output/grid_search', 'Max_depth_r2_' + route + '.csv'), index=False)
    plot = sns.catplot(x="Parameter", y="R2", data=estimator_out)
    plot.fig.suptitle("Max_depth_r2_")
    plot.savefig(os.path.join('../data/2 Output/grid_search', 'Max_depth_r2_' + route + '.png'))

    param_set['max_depth'] = None


    for item4 in param_grid['min_samples_split']:
        param_set['min_samples_split'] = item4
        rf = train_rf(data_x_train, data_y_train, param_set)
        errors_train_r2, errors_dev_r2 = test_and_metrics_rf(rf, data_x_train, data_y_train,
                                                             data_x_dev, data_y_dev)
        min_samples_split_list.append([item4, errors_dev_r2])

    estimator_out = ps.DataFrame.from_records(min_samples_split_list, columns=['Parameter', 'R2'])
    estimator_out.to_csv(os.path.join('../data/2 Output/grid_search', 'Min_samples_split_r2_' + route + '.csv'), index=False)
    plot = sns.jointplot(x="Parameter", y="R2", data=estimator_out)
    plot.fig.suptitle("Min_samples_split_r2_")
    plot.savefig(os.path.join('../data/2 Output/grid_search', 'Min_samples_split_r2_' + route + '.png'))

    param_set['min_samples_split'] = 2

    for item5 in param_grid['min_samples_leaf']:
        param_set['min_samples_leaf'] = item5
        rf = train_rf(data_x_train, data_y_train, param_set)
        errors_train_r2, errors_dev_r2 = test_and_metrics_rf(rf, data_x_train, data_y_train,
                                                             data_x_dev, data_y_dev)
        min_samples_leaf_list.append([item5, errors_dev_r2])

    estimator_out = ps.DataFrame.from_records(min_samples_leaf_list, columns=['Parameter', 'R2'])
    estimator_out.to_csv(os.path.join('../data/2 Output/grid_search', 'Min_samples_leaf_r2_' + route + '.csv'), index=False)
    plot = sns.jointplot(x="Parameter", y="R2", data=estimator_out)
    plot.fig.suptitle("Min_samples_leaf_r2_")
    plot.savefig(os.path.join('../data/2 Output/grid_search', 'Min_samples_leaf_r2_' + route + '.png'))

    param_set['min_samples_leaf'] = 1




    return 1

def scale_dataframe_columns( frame: ps.DataFrame, columnList ):
    #"""
    #Scales a Pandas DataFrame object: centers at zero, scales by std deviation
    #"""
    sdevs = frame[ columnList ].std()
    means =  frame[columnList ].mean()
    scaledframe = frame
    scaledframe[columnList ] -= means
    scaledframe[columnList ] /= sdevs
    return scaledframe, means, sdevs

if __name__ == "__main__":

    route_list = ['M180', 'M181', 'L401', 'L402', 'L403', 'L404', 'L405', 'L406', 'L407', 'M090', 'M185', 'M186'
        , 'M187', 'M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']
    # route_list = ['M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']
    # route_list = ['L402']
    # route_list = ['All']
    filter_route = True
    plt.style.use('fivethirtyeight')

    results = ps.DataFrame()
#    np.zeros((30,3))


    for route in route_list:
        run_rf(route, filter_route=filter_route)



