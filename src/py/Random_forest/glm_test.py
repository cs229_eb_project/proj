import numpy as np
import util
import pandas as ps
import statsmodels.formula.api as smf
import statsmodels.api as sm
import sklearn as sk

def main():
    data = ps.read_csv("../data/Trip_energy_usage_example.csv")

    data_x = data.loc[:, [
                             'Vehicle_Day_Route_key',
                             'Hashed_driver',
                             'Trip_total_number_Of_Boardings',
                             'Trip_Max_Occupancy',
                             'Trip_Avg_Occupancy',
                             'Trip_Within_ScheduledTime',
                             'Trip_Departure_Punctuality_Cat',
                             'Trip_Departure_Punctuality',
                             'Trip_Arrival_Punctuality_Cat',
                             'Trip_Arrival_Punctuality',
                             'Vehicle_Static_Weight',
                             'Vehicle_Brand',
                             'Vehicle_Day_Route_Trip_key',
                             'Vehicle',
                             'Route',
                             'Trip',
                             'Direction',
                             'Starttime',
                             'Subtrip',
                             'Date',
                             'Trip_Km_Travelled_DuringTrip',
                             'Trip_Energy_Per_Km',
                             'Trip_Number_Of_Measures',
                             'Time_Slot_halfhour']]
    data_x = [
        data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
        data.loc[:, 'Trip_Max_Occupancy'].replace(',', '.', regex=True).astype(float),
        data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float)
    ]
    data_x = ps.concat(data_x, axis=1)
    data_x = sm.add_constant(data_x, has_constant="skip")
    data_y = data.loc[:, "Trip_Energy_Kw_Used"].replace(',', '.', regex=True).astype(float)
    mod = sm.GLM(data_y, data_x)
    res = mod.fit()
    print(res.summary())


