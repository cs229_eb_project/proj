import numpy as np
import pandas as ps
import statsmodels.formula.api as smf
import statsmodels.api as sm
import sklearn as sk
from sklearn.impute import SimpleImputer
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from datetime import datetime as dt
import matplotlib.pyplot as plt
import os
import seaborn as sns
sns.set(style="ticks", color_codes=True)

import csv

def parse_data(filename, route, filter_route=True):
    #Takes: datafile
    #Returns: numpy array of datafile, formatted / parsed

    route_base_list = ['M180', 'M181', 'L401', 'L402', 'L403', 'L404', 'L405', 'L406', 'L407', 'M090', 'M185', 'M186',
                       'M187', 'M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']

    data = ps.read_csv(filename, parse_dates=['Date'])
#    ps.set_option('display.max_columns', 500)

    data['Date'] = ps.to_datetime(data['Date'], format='%Y-%m-%d', errors='coerce')

    data['Month'] = ps.DatetimeIndex(data['Date']).month
    data['Weekday'] = ps.DatetimeIndex(data['Date']).weekday

    # Filter regels waarbij energy per km is te laag
    data = data.loc[data['Trip_Energy_Kw_Used'] > 0.05]
    data = data.loc[data['Trip_Energy_Per_Km'] < 10]

    # Let op, for M (Amsterdam) zijn er in Augustus maar 10 regels -> eruit halen
    if route[0] == 'M':
        data = data.loc[data['Month'] != 8]
    else:
        pass

    # print("Aantal regels voor NaN drop: %s,%s" % (data.shape))
    data = data.dropna()
    # print("Aantal regels na NaN drop: %s,%s" % (data.shape))

    if filter_route:
        data = data.loc[data['Route'] == route]
        data = [
            data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
            data.loc[:, "Trip_Energy_Per_Km"].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'StartHour'],
            data.loc[:, 'Trip_Duration_Hr'],
            data.loc[:, 'Hour_Temperature'],
            data.loc[:, 'drivestyle_value'],
            data.loc[:, 'Number_of_stops'],
            data.loc[:, 'Month'],
            data.loc[:, 'Weekday']]
    else:
        data = data.loc[data['Route'].isin(route_base_list)]
        data = [
            data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
            data.loc[:, "Trip_Energy_Per_Km"].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'StartHour'],
            data.loc[:, 'Trip_Duration_Hr'],
            data.loc[:, 'Hour_Temperature'],
            data.loc[:, 'drivestyle_value'],
            data.loc[:, 'Number_of_stops'],
            data.loc[:, 'Month'],
            data.loc[:, 'Weekday'],
            data.loc[:, 'Route']
        ]


    #        data.loc[:, 'Trip_Departure_OnTime'],
    #        data.loc[:, 'Vehicle_Static_Weight'],
    #    data.loc[:, 'Route'],
    #    data.loc[:, 'Direction'],
    #        data.loc[:, 'Urban_Avg'],

    data = ps.concat(data, axis=1)
    data = data.rename(index=str, columns={"Trip_total_number_Of_Boardings": "Nr of Boardings", "Trip_Avg_Occupancy": "Avg Occupancy", "Syc_Speed_kmh": "Avg Speed   "
        , "Urban_Avg": "Avg Urbanisation", "Trip_Departure_Punctuality": "Punctuality (depart)", "Trip_Duration_Hr": "Trip duration"
        , "Number_of_stops": "Number of stops", "Trip_Km_Travelled_DuringTrip": "Distance Trip (KM)"})
    data_x = data.loc[:, data.columns != "Trip_Energy_Per_Km"]
#    data_x = ps.concat(data_x, axis=1)
    # data_x_train = sm.add_constant(data_x_train, has_constant="skip")
    data_y = data.loc[:, "Trip_Energy_Per_Km"]

    data_x = ps.concat([data_x, ps.get_dummies(data_x['StartHour'], prefix='StartHour')], axis=1)
    data_x = ps.concat([data_x, ps.get_dummies(data_x['Month'], prefix='Month')], axis=1)
    data_x = ps.concat([data_x, ps.get_dummies(data_x['Weekday'], prefix='Weekday')], axis=1)
    # data_x = ps.concat(
    #     [data_x, ps.get_dummies(data_x['Trip_Departure_OnTime'], prefix='Trip_Departure_OnTime')], axis=1)
    data_x = data_x.drop('StartHour', axis=1)
    data_x = data_x.drop('Month', axis=1)
    data_x = data_x.drop('Weekday', axis=1)

    if filter_route:
        pass
    else:
        data_x = ps.concat([data_x, ps.get_dummies(data_x['Route'], prefix='Route')], axis=1)
        data_x = data_x.drop('Route', axis=1)
    # data_x = data_x.drop('Trip_Departure_OnTime', axis=1)
#    print(data_x.head(5))

    # Get the headers as a feature list
    feature_list = list(data_x.columns)


    data_x = data_x.values
    data_y = data_y.values

    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    data_x = imp.fit_transform(data_x)

    return data_x, data_y, feature_list

def train_rf(data_x_train,data_y_train, route):

    param_data = ps.read_csv("../data/0 Raw/Output_group.csv", delimiter=';')
    param_data = param_data.loc[param_data['Line'] == route]
    param_data_dict = {}
    for row in param_data.iterrows():
        if row[1].loc['Parameter_Name_Opt'] == 'Max_features':
            if row[1].loc['Best_Parameter_Value'] == 'auto':
                param_data_dict[row[1].loc['Parameter_Name_Opt']]=row[1].loc['Best_Parameter_Value']
            else:
                param_data_dict[row[1].loc['Parameter_Name_Opt']] = int(row[1].loc['Best_Parameter_Value'])
        else:
            param_data_dict[row[1].loc['Parameter_Name_Opt']] = int(row[1].loc['Best_Parameter_Value'])
    # rf = RandomForestRegressor(n_estimators=1000, random_state=42, n_jobs=-1) # param_data_dict['Nr_estimator'] param_data_dict['Max_features']
    # rf = RandomForestRegressor(n_estimators=param_data_dict['Nr_estimator_Upperlimit'], random_state=42, n_jobs=-1, max_features=param_data_dict['Max_features'], bootstrap=True, min_samples_leaf=param_data_dict['Min_samples_leaf'], min_samples_split=param_data_dict['Min_samples_split'],max_depth=param_data_dict['Max_depth'])
    rf = RandomForestRegressor(n_estimators=param_data_dict['Nr_estimator'], random_state=42, n_jobs=1, max_features=param_data_dict['Max_features'], bootstrap=True, min_samples_leaf=param_data_dict['Min_samples_leaf'], min_samples_split=param_data_dict['Min_samples_split'],max_depth=param_data_dict['Max_depth'])

    # rf_random.best_params_
    rf.fit(data_x_train, data_y_train)

    return rf

def test_and_metrics_rf(rf, feature_list, data_x_train, data_y_train, data_x_dev, data_y_dev, data_x_test, data_y_test, route):

    Metrics_array = []#[None, None]*8 #np.zeros((8,2))
    Predict_array = []#[None, None]*4  #np.zeros((4,2))

    predictions_train = rf.predict(data_x_train)
    predictions_dev = rf.predict(data_x_dev)
    try:
        predictions_test = rf.predict(data_x_test)
    except:
        print("No test result for: %s" % route)
    errors_train = metrics.mean_squared_error(data_y_train, predictions_train)  # abs(predictions - data_y_dev)
    errors_dev = metrics.mean_squared_error(data_y_dev, predictions_dev)  # abs(predictions - data_y_dev)
    try:
        errors_test = metrics.mean_squared_error(data_y_test, predictions_test)
        errors_test_r2 = metrics.r2_score(data_y_test, predictions_test)  # abs(predictions - data_y_dev)
    except:
        None
    errors_train_r2 = metrics.r2_score(data_y_train, predictions_train)  # abs(predictions - data_y_dev)
    errors_dev_r2 = metrics.r2_score(data_y_dev, predictions_dev)  # abs(predictions - data_y_dev)
    Predict_array.append(['Train',list(zip(*[np.transpose(data_y_train).tolist(), np.transpose(predictions_train).tolist()]))])
    # Predict_array[1] = ['Dev', [data_y_dev.transpose, predictions_dev.transpose]]
    Predict_array.append(['Dev', list(zip(*[np.transpose(data_y_dev).tolist(), np.transpose(predictions_dev).tolist()]))])
    # Metrics_array[0] = ['MSE train', errors_train]
    Metrics_array.append(['MSE train', round(np.mean(errors_train), 4)])
    Metrics_array.append(['MSE dev', round(np.mean(errors_dev), 4)])
    Metrics_array.append(['R2 train', round(np.mean(errors_train_r2), 4)])
    Metrics_array.append(['r2 dev', round(np.mean(errors_dev_r2), 4)])
    try:
        Predict_array.append(
            ['Test', list(zip(*[np.transpose(data_y_test).tolist(), np.transpose(predictions_test).tolist()]))])
        Metrics_array.append(['MSE test', round(np.mean(errors_test), 4)])
        Metrics_array.append(['r2 test', round(np.mean(errors_test_r2), 4)])
    except:
        None
    # Print out the mean absolute error (mae)
    # print('MSE', round(np.mean(errors_train), 4), 'kwh.')
    # print('MSE', round(np.mean(errors_dev), 4), 'kwh.')
    # print('R2', round(np.mean(errors_train_r2), 4))
    # print('R2', round(np.mean(errors_dev_r2), 4))

    predictions_train_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': predictions_train}), ['Column1'])[0].values
    predictions_dev_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': predictions_dev}), ['Column1'])[0].values
    try:
        predictions_test_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': predictions_test}), ['Column1'])[
            0].values
        data_y_test_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': data_y_test}), ['Column1'])[0].values
        errors_test_scaled = metrics.mean_squared_error(data_y_test_scaled,
                                                        predictions_test_scaled)
    except:
        None
    data_y_train_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': data_y_train}), ['Column1'])[0].values #['Trip_Energy_Per_Km']
    data_y_dev_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': data_y_dev}), ['Column1'])[0].values

    errors_train_scaled = metrics.mean_squared_error(data_y_train_scaled, predictions_train_scaled)  # abs(predictions - data_y_dev)
    errors_dev_scaled = metrics.mean_squared_error(data_y_dev_scaled, predictions_dev_scaled)   # abs(predictions - data_y_dev)
      # abs(predictions - data_y_dev)
    # errors_train_r2_scaled = metrics.r2_score(data_y_train_scaled, predictions_train_scaled)   # abs(predictions - data_y_dev)
    # errors_dev_r2_scaled = metrics.r2_score(data_y_dev_scaled, predictions_dev_scaled)   # abs(predictions - data_y_dev)

    # print('MSE scaled', round(np.mean(errors_train_scaled), 4), 'kwh.')
    # print('MSE scaled', round(np.mean(errors_dev_scaled), 4), 'kwh.')
    # print('R2 scaled', round(np.mean(errors_train_r2_scaled), 4))
    # print('R2 scaled', round(np.mean(errors_dev_r2_scaled), 4))
    Predict_array.append(['Train - scaled', list(zip(*[np.transpose(data_y_train_scaled).tolist(), np.transpose(predictions_train_scaled).tolist()]))])
    Predict_array.append(['Dev - scaled', list(zip(*[np.transpose(data_y_dev_scaled).tolist(), np.transpose(predictions_dev_scaled).tolist()]))])
    Metrics_array.append(['MSE train - scaled', round(np.mean(errors_train_scaled), 4)])
    Metrics_array.append(['MSE dev - scaled', round(np.mean(errors_dev_scaled), 4)])
    # Metrics_array.append(['R2 train - scaled', round(np.mean(errors_train_r2_scaled), 4)])
    # Metrics_array.append(['r2 dev - scaled', round(np.mean(errors_dev_r2_scaled), 4)])
    try:
        Predict_array.append(['Test - scaled', list(
            zip(*[np.transpose(data_y_test_scaled).tolist(), np.transpose(predictions_test_scaled).tolist()]))])
        Metrics_array.append(['MSE test - scaled', round(np.mean(errors_test_scaled), 4)])
    except:
        None
    # Get numerical feature importances
    importances = list(rf.feature_importances_)
    # List of tuples with variable and importance
    feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
    # Sort the feature importances by most important first
    feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
    # Print out the feature and importances

    feature_importances_zip = list(zip(*feature_importances))

    return feature_importances, feature_importances_zip, Metrics_array, Predict_array


def run_rf(route, filter_route=True):
    data_x_train, data_y_train, feature_list = parse_data("../data/0 Raw/Trip_energy_usage_processed_train.csv", route,
                                                          filter_route)
    data_x_dev, data_y_dev, feature_list_dev = parse_data("../data/0 Raw/Trip_energy_usage_processed_dev.csv", route,
                                                           filter_route)
    data_x_test, data_y_test, feature_list_test = parse_data("../data/0 Raw/Trip_energy_usage_processed_test.csv", route,
                                                           filter_route)

    print(route)

    print("data loaded, starting to train")

    rf = train_rf(data_x_train, data_y_train, route)

    print("Testing predictions, producing metric")

    feature_importances, feature_importances_zip, Metrics_List,Predict_array  = test_and_metrics_rf(rf, feature_list, data_x_train, data_y_train, data_x_dev, data_y_dev, data_x_test, data_y_test, route)

    return rf, feature_importances, feature_importances_zip, Metrics_List, Predict_array

def scale_dataframe_columns( frame: ps.DataFrame, columnList ):
    #"""
    #Scales a Pandas DataFrame object: centers at zero, scales by std deviation
    #"""
    sdevs = frame[ columnList ].std()
    means =  frame[columnList ].mean()
    scaledframe = frame
    scaledframe[columnList ] -= means
    scaledframe[columnList ] /= sdevs
    return scaledframe, means, sdevs

if __name__ == "__main__":

    route_list = ['M180', 'M181', 'L401', 'L402', 'L403', 'L404', 'L405', 'L406', 'L407', 'M090', 'M185', 'M186'
        , 'M187', 'M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']
    # route_list = ['L402', 'L403', 'L404', 'L405', 'L406', 'L407', 'M090', 'M185', 'M186'
    #     , 'M187', 'M190', 'M191', 'M194', 'M198', 'M199', 'M242', 'M342', 'M347', 'M357']
    # route_list = ['L401']
    # route_list = ['All']
    filter_route = True
    plt.style.use('fivethirtyeight')


    par_run = 'line-specific-3_'

    results = ps.DataFrame()
#    np.zeros((30,3))


    for route in route_list:
        rf, feature_importances, feature_importances_zip, Metrics, pred = run_rf(route, filter_route=filter_route)

        pred_train_out = ps.DataFrame.from_records(pred[0][1], columns=['True labels', 'Predictions'])
        pred_dev_out = ps.DataFrame.from_records(pred[1][1], columns=['True labels', 'Predictions'])
        test_dev_out = ps.DataFrame.from_records(pred[2][1], columns=['True labels', 'Predictions'])
        pred_train_out.to_csv(os.path.join('../data/2 Output', 'pred_train_rf_' + par_run + route + '.csv'), index=False)
        pred_dev_out.to_csv(os.path.join('../data/2 Output', 'pred_dev_rf_' + par_run + route + '.csv'), index=False)
        test_dev_out.to_csv(os.path.join('../data/2 Output', 'test_dev_rf_' + par_run + route + '.csv'), index=False)
        train_plot = sns.jointplot(x="True labels", y="Predictions", data=pred_train_out)
        train_plot.fig.suptitle("Train set")
        train_plot.savefig(os.path.join('../data/2 Output', 'pred_train_rf_' + par_run + route + '.png'))
        dev_ploit = sns.jointplot(x="True labels", y="Predictions", data=pred_dev_out)
        dev_ploit.fig.suptitle("Dev set")
        dev_ploit.savefig(os.path.join('../data/2 Output', 'pred_dev_rf_' + par_run + route + '.png'))
        test_ploit = sns.jointplot(x="True labels", y="Predictions", data=pred_dev_out)
        test_ploit.fig.suptitle("Test set")
        test_ploit.savefig(os.path.join('../data/2 Output', 'test_dev_rf_' + par_run + route + '.png'))
        # temp_results = ps.DataFrame()
        # metrics_out = ps.DataFrame()
        metrics_out = ps.DataFrame.from_records(Metrics,columns=['Metric','Output'])
        metrics_out.to_csv(os.path.join('../data/2 Output', 'metrics_rf_' + par_run + route + '.csv'), index=False)
#        [print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];

#        plt.figure()
#        x_values = list(range(len(feature_importances_zip[0])))
        temp_results = ps.DataFrame.from_records(feature_importances,columns=['Feature','Importance']).head(10)
        temp_results.to_csv(os.path.join('../data/2 Output', 'export_rf_' + par_run + route + '.csv'), index=False)
        temp_results['Route'] = route
#         results = results.append(ps.DataFrame({
#             'Feature': feature_importances[0][:10],
#             'Importance': feature_importances[1][:10]
#         }))
        results = results.append(temp_results)
 #       x_values = list(range(10))
        # # Make a bar chart
        # plt.bar(x_values, feature_importances_zip[1][:10], orientation='vertical')
        # # Tick labels for x axis
        # plt.xticks(x_values, feature_importances_zip[0][:10], rotation='45')
        # # Axis labels and title
        # plt.ylabel('Importance');
        # plt.xlabel('Variable');
        # plt.title('Variable Importances');


    # Set the style



        scatterplot = sns.catplot(x="Feature", y="Importance", hue="Route", data=results)
        scatterplot.set_xticklabels(rotation=30)
        plt.savefig(os.path.join('../data/2 Output', 'export_rf_' + par_run + route + '.png'))
    # plt.show()

#    ax2 = results.plot.scatter(x='Feature', y='Importance',c='Route', colormap='viridis')
#    plt.show()

    # list of x locations for plotting
    # result_array = results.values
    # color_array = np.zeros(len(result_array))
    #
    # x = result_array[0]
    # y = result_array[1]
    #
    # for i in range(len(result_array)):
    #     if results[1][i] == 'M180':
    #         color_array[i] = 'b'
    #     elif results[1][i] == 'M181':
    #         color_array[i] = 'g'
    #     elif results[1][i] == 'L401':
    #         color_array[i] = 'r'
    #     else: #'L405'
    #         color_array[i] = 'k'
    #
    # for i in range(len(x)):
    #     plt.plot(x[i], y[i], 'ko', color=color_array[i])
    # plt.show()

