import numpy as np
import util
import pandas as ps
import statsmodels.formula.api as smf
import statsmodels.api as sm


def main():
    data_train = ps.read_csv("../data/Trip_energy_usage_processed_train.csv")
    data_dev = ps.read_csv("../data/Trip_energy_usage_processed_dev.csv")

    data_x = data_train[:, [
                             'Driver',
                             'Trip_Avg_Occupancy',
                             'Trip_total_number_Of_Boardings',
                             'Trip_Max_Occupancy',
                             'Trip_Within_ScheduledTime',
                             'Trip_Departure_OnTime',
                             'Trip_Departure_Punctuality',
                             'Trip_Arrival_OnTime',
                             'Trip_Arrival_Punctuality',
                             'Vehicle_Static_Weight',
                             'Vehicle_Brand',
                             'RouteDate',
                             'RouteKey',
                             'RouteNo',
                             'RouteDate2',
                             'RouteKey2',
                             'RouteTrip',
                             'Vehicle',
                             'Route',
                             'Trip',
                             'Direction',
                             'Starttime',
                             'StartHour',
                             'Subtrip',
                             'Date',
                             'Trip_Km_Travelled_DuringTrip',
                             'Syc_Speed_kmh',
                             'Trip_Duration_Hr',
                             #'Trip_Energy_Kw_Used',
                             #'Trip_Energy_Per_Km',
                             'Trip_Number_Of_Measures',
                             'Time_Slot_halfhour',
                             'TimeSlotHour',
                             'Hour_Temperature',
                             'drivestyle_letter',
                             'drivestyle_value',
                             'Number_of_stops',
                             'Urban_Avg'#,
                           #  'Hashed_driver'
                           ]].values
    data_y = data_train.loc[:, "Trip_Energy_Kw_Used"]
    mod = sm.GLM(data_y, data_x)
    res = mod.fit()
    print(res.summary())
