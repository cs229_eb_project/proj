import pandas as pd
import numpy as np
from sklearn import datasets, linear_model
import busdata

# calculate R2
def rsquared(true, predicted):
	ss_res = np.sum(np.square(true-predicted))
	ss_tot = np.sum(np.square(true-np.mean(true)))
	return 1 - ss_res/ss_tot

# read all data
data = busdata.read_all(".", verbose=False)

# regression for each route
for route in (busdata.get_routes(data)):
	(train_x, train_y, dev_x, dev_y) = busdata.get_dataset(data, route, verbose=False)

	print("{0:s}	{1:d}".format(route, train_x.shape[0]))
	print("y-train  min={0:f} max={1:f} mean={2:f} var={3:f}".format(np.min(train_y), np.max(train_y), np.mean(train_y), np.var(train_y)))
	print("y-dev    min={0:f} max={1:f} mean={2:f} var={3:f}".format(np.min(dev_y), np.max(dev_y), np.mean(dev_y), np.var(dev_y)))
