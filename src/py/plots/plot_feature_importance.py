from sys import platform as sys_pf
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import io
import plot_settings

colnames_rtree = {
	'Avg Occupancy''v''Avg Speed':'Avg Occupancy/Avg Speed',
	'Avg Urbanisation':'Urbanization',
	'Distance Trip (KM)':'Distance',
	'Hour_Temperature':'Temperature',
	'Nr of Boardings':'#Boardings',
	'Number of stops':'#Stops',
	'Punctuality (depart)':'Punctuality',
	'Trip duration':'Trip duration',
	'drivestyle_value':'Drive style',
	'Month':'Month',
	'Avg Speed':'Avg. speed',
	'StartHour':'Time',
	'Avg Occupancy':'Avg. occupancy'
	}

colnames_other = {
	'Hour_Temperature':'Temperature',
	'Month':'Month',
	'Number_of_stops':'#Stops',
	'StartHour':'Time',
	'Syc_Speed_kmh':'Avg. speed',
	'Trip_Avg_Occupancy':'Avg Occupancy',
	'Trip_Departure_Punctuality':'Punctuality',
	'Trip_Duration_Hr':'Trip duration',
	'Trip_Km_Travelled_DuringTrip':'Distance',
	'Trip_total_number_Of_Boardings':'#Boardings',
	'Urban_Avg':'Urbanization',
	'Weekday':'Weekday',
	'drivestyle_value':'Drive style'
	}

def read_file(data_dir, fname, index_col=None):
	dt = pd.read_csv('{}/{}'.format(data_dir, fname), index_col=index_col)
	return dt

def read_all(data_dir):
	# load others
	dt1 = pd.DataFrame(read_file(data_dir, "varImp_glmnet.csv", index_col=0))
	dt1a = pd.DataFrame(read_file(data_dir, "varImp_glmnet_mean.csv", index_col=0))
	dt1b = pd.DataFrame(read_file(data_dir, "varImp_glmnet_sum.csv", index_col=0))
	dt1c = pd.DataFrame(read_file(data_dir, "varImp_glmnet_max.csv", index_col=0))
	dt2 = pd.DataFrame(read_file(data_dir, "varImp_lm_linear.csv", index_col=0))
	dt3 = pd.DataFrame(read_file(data_dir, "varImp_lm_quadratic.csv", index_col=0))
	dt1['model'] = 'glmnet'
	dt1a['model'] = 'glmnet-mean'
	dt1b['model'] = 'glmnet-sum'
	dt1c['model'] = 'glmnet-max'
	dt2['model'] = 'lm.linear'
	dt3['model'] = 'lm.quadratic'
	dt = pd.concat([dt1, dt1a, dt1b, dt1c, dt2, dt3], sort=False)
	# fix headings
	dct = dict(colnames_other)
	for k1 in colnames_other:
		for k2 in colnames_other:
			dct["{}:{}".format(k1, k2)] = "{}/{}".format(dct[k1], dct[k2])
	dt = dt.rename(index=str, columns=dct)
	# load rtree
	rtree = pd.DataFrame(read_file(data_dir, "varImp_rtree.csv"))
	dt4 = pd.DataFrame()
	for route in rtree['Line'].unique():
		d = rtree.loc[rtree['Line'] == route,['Feature','Average importance']]
		dt4a = pd.DataFrame(dict(zip(d['Feature'],d['Average importance'])), index=[route])
		#dt4= dt4.append(dict(zip(d['Feature'],d['Average importance'])), ignore_index=True)
		dt4 = pd.concat([dt4, dt4a], sort=False)
	dt4['model'] = 'rtree'
	# fix month
	v = dt4[[col for col in dt4.columns if col.startswith("Month")]].sum(1)
	dt4 = dt4[[col for col in dt4.columns if not col.startswith("Month")]]
	dt4['Month'] = v
	# fix starthour
	v = dt4[[col for col in dt4.columns if col.startswith("StartHour")]].sum(1)
	dt4 = dt4[[col for col in dt4.columns if not col.startswith("StartHour")]]
	dt4['StartHour'] = v
	dt4 = dt4.rename(index=str, columns=colnames_rtree)
	# concat
	dt = pd.concat([dt, dt4], sort=False)
	# normalize each row
	model = dt['model']
	dt_sum = dt[[col for col in dt.columns if col!='model']].sum(axis=1)
	dt = dt[[col for col in dt.columns if col!='model' and col!='route']].div(dt_sum, axis=0)
	dt['model'] = model
	dt = dt.fillna(0)
	return dt

def init():
	dt = read_all(".")
	return dt

data = init()

palette = plot_settings.plot_init()

sns.set()
sns.set_palette(palette)
sns.set(style="ticks", color_codes=True, rc={"lines.linewidth": 1.5})
sns.set(font_scale=1.4)

types = {
	'rtree':'Random Forest',
	'lm.linear':'$lm$',
	'lm.quadratic':'$lm(x^2)$',
	'glmnet':'$lm\ (x^2,reg)$',
	'glmnet-mean':'$lm\ (x^2,reg),mean$',
	'glmnet-sum':'$lm\ (x^2,reg),sum$',
	'glmnet-max':'$lm\ (x^2,reg),max$'
	}

for type, typename in types.items():

	plt.figure()
	columns = [ x for x in data.columns ]
	columns.remove('model')

	dt = data[data['model']==type].drop(columns='model').T
	dt_sum = dt.mean(axis=1, skipna=True).sort_values(ascending=False)
	dt_sum = dt_sum[dt_sum>0]
	dt = dt.loc[dt_sum.index]
	dt = dt.iloc[0:min(12, dt.shape[0]),]
	dt = pd.melt(dt.T)

	plot = sns.boxplot(x='value', y='variable', data=dt, whis='range',
		showmeans=False, linewidth=1.0, notch=False, width=.8,
		palette=palette)
	sns.despine()
	sns.swarmplot(x='value', y='variable', data=dt,
		      size=2, color=".3", linewidth=0)
	plt.subplots_adjust(left=0.4, bottom=0.15)
	plot.set(xlabel='$\%Weight$', xlim=None, ylabel=None)
	plot.get_xaxis().set_major_formatter(
	    matplotlib.ticker.FuncFormatter(lambda x, p: '{0:.0%}'.format(x)))
	plot.axes.set_title('Feature Importance for {}'.format(typename),
		fontsize=24, y=1, x=.1)

	#plot.axvline(.25, linestyle='dotted', lw=1)
	#plot.axvline(.50, linestyle='dotted', lw=1)
	#plot.axvline(.75, linestyle='dotted', lw=1)

	plt.savefig('plot_feature_importance_{}.png'.format(type), dpi=400)

plt.show()
