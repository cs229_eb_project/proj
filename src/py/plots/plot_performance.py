from sys import platform as sys_pf
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import io
import plot_settings

def read_file(data_dir, fname):
	dt = pd.read_csv('{}/{}'.format(data_dir, fname), index_col=0)
	return dt

def read_all(data_dir):
	dt = pd.concat([
		read_file(data_dir, "rsquared_lm_glmnet.csv"),
		read_file(data_dir, "rsquared_lm_linear.csv"),
		read_file(data_dir, "rsquared_lm_quadratic.csv"),
		read_file(data_dir, "rsquared_nn.csv"),
		read_file(data_dir, "rsquared_svm.csv") ], sort=False)
	rtree = pd.DataFrame(read_file(data_dir, "rsquared_rtree.csv"))
	rtree = rtree.rename(index=str, columns={'R-squared':'R2dev'})
	rtree['model'] = 'rtree'
	dt = pd.concat([dt, rtree], sort=False)
	dt['R2dev'] = pd.to_numeric(dt['R2dev'])
	return dt

def init():
	sns.set(style="ticks", color_codes=True)
	dt = read_all(".")
	return dt

data = init()

sns.set()

palette = plot_settings.plot_init()

labels = {
	'rtree':'$R.Tree$', 
	'svm':'$SVM$', 
	'lm.glmnet':'$lm (x^2,reg)$',
	'nn':'$N.Net$',
	'lm.quadratic':'$lm (x^2)$', 
	'lm.linear':'$lm$'
	}
o = data.groupby('model').agg('median')
o = o.sort_values(['R2dev'],ascending=False)
print(o)
data = data[['R2dev', 'model']]
data['model'] = [labels[l] for l in data['model']]
o = [labels[i] for i in o.index]

plot = sns.boxplot(x='R2dev', y='model', data=data, order=o, showmeans=True, palette=palette)
plt.subplots_adjust(left=0.2, bottom=.15)
sns.swarmplot(x='R2dev', y='model', data=data,
	      size=2, color=".75", linewidth=0, order=o)
plot.set(xlabel='$R^2\ (dev\ set)$', xlim=(0,1), ylabel='',
	title='Prediction accuracy across routes', xticks=[0,.25,.5,.75,1])
plot.axvline(.25, linestyle='dotted', lw=1)
plot.axvline(.50, linestyle='dotted', lw=1)
plot.axvline(.75, linestyle='dotted', lw=1)
plot.axes.set_title('Prediction Accuracy', fontsize=25)
plot.set_xlabel("$R^2\ (dev\ set)$") #, fontsize=20)
#plot.set_ylabel("", fontsize=30)
plt.savefig('plot_performance.png', dpi=400)

plt.show()

# input("Press enter...")
