import pandas as pd
import numpy as np
from sklearn import datasets, linear_model
import busdata
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import io
import plot_settings


plt.figure()

data = pd.read_csv('rtree_tuning.csv', index_col=None)

palette = plot_settings.plot_init()

sns.set(font_scale=1.4)

plot = sns.lineplot(x="maxFeatures", y="R2", data=data, linewidth=6)
sns.despine()
plot.set(xlabel='#Features', xlim=None, ylabel='$R^2$')

plt.subplots_adjust(left=0.2, bottom=0.15)
plot.axes.set_title('Random Tree Tuning',
	fontsize=24, y=1, x=.45)
plot.set_xlabel("Number of features per tree") #, fontsize=14)
plot.set_ylabel("$R^2$", fontsize=14)
#plot.tick_params(labelsize=5)

plt.savefig('plot_rtree_tuning.png', dpi=400)

plt.show()

