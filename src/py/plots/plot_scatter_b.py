import pandas as pd
import numpy as np
from sklearn import datasets, linear_model
import busdata
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import io
import plot_settings


routes = [ "L402", "L407" ]

for route in routes:

	plt.figure()

	# pd.options.mode.chained_assignment = 'raise'

	# calculate R2
	def rsquared(true, predicted):
		ss_res = np.sum(np.square(true-predicted))
		ss_tot = np.sum(np.square(true-np.mean(true)))
		return 1 - ss_res/ss_tot

	# read all data
	data = busdata.read_all("../../data/20181205/processed", verbose=False)

	(train_x, train_y, dev_x, dev_y) = busdata.get_dataset(data, route, verbose=False, denormalize=True)

	d = pd.concat([train_y, train_x[['Trip_Km_Travelled_DuringTrip']]], axis=1)
	d.loc[:, 'Trip_Energy_Use'] = d.loc[:, 'Trip_Km_Travelled_DuringTrip'] * d.loc[:, 'Trip_Energy_Per_Km']

	palette = plot_settings.plot_init()

	#d.to_csv("scatter.cvs")

	#print(d[d.loc[:, 'Trip_Km_Travelled_DuringTrip']<5])
	sns.set(font_scale=1.4)
	plot = sns.scatterplot(y='Trip_Energy_Per_Km', x='Trip_Km_Travelled_DuringTrip',
		data=d.loc[:, ['Trip_Energy_Per_Km','Trip_Km_Travelled_DuringTrip']],
		s=5, marker='o', alpha=0.1, edgecolors='k', color='black')
	matplotlib.rcParams['lines.markeredgewidth'] = 1
	sns.despine()
	plot.set(xlabel='Distance Travelled', xlim=None, ylabel='Kwh')

	a = d.loc[:, 'Trip_Km_Travelled_DuringTrip'].quantile(q=.9995)
	b = d.loc[:, 'Trip_Energy_Per_Km'].quantile(q=.9995)

	plt.ylim(0, b)
	plt.xlim(0, a)

	plt.subplots_adjust(left=0.15, bottom=0.15)
	plot.axes.set_title('Energy Use by Distance, Route {}'.format(route),
		fontsize=24, y=1, x=.45)
	plot.set_xlabel("Km") #, fontsize=14)
	plot.set_ylabel("Kwh) #", fontsize=14)
	#plot.tick_params(labelsize=5)

	plt.savefig('plot_energy_pkm_use_{}.png'.format(route), dpi=400)

plt.show()

