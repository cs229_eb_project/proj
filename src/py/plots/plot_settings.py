import seaborn as sns


colors_dec = [
	[ 138, 162, 186 ],
	[ 230, 57, 54 ],
	[ 163, 217, 131 ],
	[ 119, 129, 139 ]
	]

colors_scatter = [ 50, 50, 50 ]

def plot_init():
	colors_hex = []
	for i in colors_dec:
		colors_hex.append("#{0:02X}{1:02X}{2:02X}".format(i[0], i[1], i[2]).lower())
	#sns.set_palette(sns.color_palette("GnBu_d"))
	palette = sns.color_palette(colors_hex)
	sns.set_palette(palette)
	sns.set(font_scale=1.4, font='Open Sans')
	return palette

def scatter_col():
	col =  "#{0:02X}{0:02X}{0:02X}".format(colors_scatter[0],
		colors_scatter[1], colors_scatter[2]).lower()
	return col

def scatter_col_palette():
	return sns.color_palette([scatter_col()])
