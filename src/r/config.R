root = "/Users/putt/cs229/project/proj"
if (!dir.exists(root)) {
  root = "/Users/putt/Documents/Classes/cs229/project/proj/src/r"
  if (!dir.exists(root)) {
    root = "/home/putt/proj"
  }
}
version = "20181205"

dir_code = paste0(root, "/src/r")
dir_data = paste0(root, "/data/", version)
output_dir = paste0(root, "/output")
output_dir_data = paste0(dir_data, "/processed")

# setwd(dir_code)

if (!file.exists(output_dir_data)) dir.create(output_dir_data)
if (!file.exists(output_dir)) dir.create(output_dir)

file_raw = "Trip_energy_usage.csv"
file_dev = "Trip_energy_usage_processed_dev.csv"
file_train = "Trip_energy_usage_processed_train.csv"
file_test = "Trip_energy_usage_processed_test.csv"

all_routes = c("L400","L401","L402","L403","L404","L405","L406","L407","M042","M047","M057","M090","M095","M180","M181","M185","M186","M187","M190","M191","M194","M195","M198","M199","M242","M287","M301","M342","M347","M348","M357","M358","M612")
drop_routes = c("M358", "M348", "M057", "M612", "M287", "M095", "M042", "M047", "M301")
routes = setdiff(all_routes, drop_routes)

# routes = c("M181","L402")
# drop_routes = setdiff(all_routes, routes)
