output_dir = "../../output"
dir_code = "/Users/putt/Documents/Classes/cs229/project/proj/src/r"

if (file.exists(dir_code)) setwd(dir_code)
if (!file.exists(output_dir)) dir.create(output_dir)

fname = sprintf("%s/models_lm_all.pdf", output_dir)
rmarkdown::render("models_lm_all.Rmd", output_file=fname)
