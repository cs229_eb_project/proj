output_dir = "../../output"
dir_code = "/Users/putt/cs229/project/proj/src/r"

if (file.exists(dir_code)) setwd(dir_code)
if (!file.exists(output_dir)) dir.create(output_dir)

fname = sprintf("%s/models_svm.pdf", output_dir)
rmarkdown::render("models_svm.Rmd", output_file=fname)
