import argparse

from glm_test import main as glm_test

parser = argparse.ArgumentParser()
parser.add_argument('p_num', nargs='?', type=int, default=0,
                    help='Scenario number to run, 0 for all problems.')
args = parser.parse_args()

# Scenario 1
if args.p_num == 0 or args.p_num == 1:
    glm_test()
