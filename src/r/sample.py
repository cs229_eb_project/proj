import pandas as pd
import numpy as np
from sklearn import datasets, linear_model
import busdata

# calculate R2
def rsquared(true, predicted):
	ss_res = np.sum(np.square(true-predicted))
	ss_tot = np.sum(np.square(true-np.mean(true)))
	return 1 - ss_res/ss_tot

# read all data
data = busdata.read_all(".", verbose=False)

# regression for each route
for route in (busdata.get_routes(data)):
	(train_x, train_y, dev_x, dev_y) = busdata.get_dataset(data, route, verbose=False)

	train_x = pd.get_dummies(train_x)
	dev_x = pd.get_dummies(dev_x)

	regr = linear_model.LinearRegression()
	fit = regr.fit(train_x, train_y)
	train_y_pred = regr.predict(train_x)
	dev_y_pred = regr.predict(dev_x)
	print("Linear regression for route [{2}] : R-squared training set={0:.6f}  dev set={1:.6f}"
		.format(rsquared(train_y, train_y_pred), rsquared(dev_y, dev_y_pred), route))

# let's check if we get the original data back if we revert normalization (for one record)
# (train_x, train_y, dev_x, dev_y) = busdata.get_dataset(data, "L405", keep_y=True, keep_route=True, denormalize=True)
# print(train_x[234:235].transpose())
