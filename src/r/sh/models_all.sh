
mkdir -p ../../output/old
mv -f ../../output/*.* ../../output/old

echo "$0 starting with nohup (press CTRL^C to return to shell)"
date
echo ""
echo ""

nohup Rscript models_all.R & 2>&1

echo ""
echo ""
date
echo "$0 completed."

tail -f nohup.out
