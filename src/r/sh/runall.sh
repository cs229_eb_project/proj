#! /bin/sh -f

echo "$0 starting "
date
echo ""
echo ""

mkdir -p ../../output/old
mv -f ../../output/*.* ../../output/old
Rscript runall.R 2>&1

echo ""
echo ""
date
echo "$0 completed."
echo ""
echo ""
echo "shutting down ..."
echo ""

sudo shutdown
